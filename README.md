Source code associated with the article

> Dual spindle formation in zygotes keeps parental genomes apart in early mammalian embryos

> J. Reichmann, B. Nijmeijer, M.J. Hossain, M. Eguren, I. Schneider, A.Z. Politi, M.J. Roberti, L. Hufnagel, T. Hiiragi, J. Ellenberg
[BioRxiv](https://www.biorxiv.org/content/early/2018/04/16/198275)

## EB3tracking
Code used to process the MT plus-end marker EB3 and compute the clustered MT comets directions.
