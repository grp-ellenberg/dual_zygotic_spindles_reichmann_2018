---
title: "Show EB3 Comets in zygote. Reichman et al. 2018"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
require('R.matlab') 
require('ggplot2')
library('MASS')
library('spatstat')
library('akima')
library('reshape2')
library('base')
library('proxy')
library('kernlab')
library('dbscan')
library('RSpectra')
library('RColorBrewer')
library('spdep')

source('show_comets_fun.R')

stage.pro <- 'microtubuleball'
stage.prometa <- 'prometaphase'
stage.meta <- 'metaphase'
stage <- stage.meta
datadir <- file.path(dirname(getwd()), 'results', stage)
# rotation angles. this is only a visual aid
if (stage == 'metaphase')
  rotate_angles <- c(0.48, -1.4, 0.4, -1.4)
if (stage == 'prometaphase')
  rotate_angles <- c( 0,  0.89, -2, 1.3,  -2.6, -pi, 0)
if (stage == 'microtubuleball')
  rotate_angles <- c(2.4832, 0.6514, 0.3627)

load(file = file.path(datadir, 'spindle_prnd.RData'))
load(file = file.path(datadir,'eigenvectors_0.55.RData'))
all_zygotes_dir <- dir(datadir, full.names = TRUE)
all_zygotes_dir <- all_zygotes_dir[dir.exists(all_zygotes_dir)]
zygotes_ids <-seq_along(all_zygotes_dir)
image.width <- 4
image.height <- 4
alphavalue = 0.3
maxtrack <- 100000
maxX <- rep(250, length(spindle_prnd))
maxY <- rep(250, length(spindle_prnd))
if (stage == stage.meta)
  cell <- c(1,2,3)
if (stage == stage.pro)
  cell <- c(1,2,3)
if (stage== stage.prometa)
  cell <- c(1,2,4)

colorUse <- c('#FF00FF', '#00FF00', '#007FFF')

#colortools::opposite("magenta")
```



```{r assigning the clusters largest cluster is first}

clust.n <- 3  # number of cluster to use
clust.max <- 6 # maximum number of cluster to verify number of clusters
clust.max <- min(clust.max, dim(evL[[1]]$vectors)[2])
for (i in zygotes_ids) {
  # if check = TRUE k mean with 1-clust.max is tested
  km <- kmean_assign(eigen = evL[[i]], clustn = clust.n, clustmax = clust.max, check = FALSE)
  spindle_prnd[[i]]$vec$clst <- km$cluster
}

#  Prune using additional step of density based clustering
spindle_prnd_db <- list()
frac.low  <- 0.7
frac.high <- 0.8
eps.low <- 1
eps.high <- 10
for (i in zygotes_ids[cell]) {
  frac <- 0
  eps.mid <- (eps.low + eps.high)/2
  eps.high <- 10
  eps.low <- 1
  while(frac < frac.low  | frac > frac.high) {
    idx_clst<- c()
    spindle_prnd_db[[i]] <- spindle_prnd[[i]]
    
  
    for (cls in seq(1, clust.n)){
      subVec <- subset(spindle_prnd[[i]]$vec, clst == cls)
      idx <- dbdenoise(cbind(subVec$centPos.x, subVec$centPos.y), eps = eps.mid, plotimg = FALSE)
      idx_clst <- c(idx_clst, which(spindle_prnd[[i]]$vec$clst == cls)[idx])
    }
    
    spindle_prnd_db[[i]]$vec <- spindle_prnd_db[[i]]$vec[idx_clst, ]
    frac <- length(idx_clst)/length(spindle_prnd[[i]]$vec$clst)
    
    if ( frac < frac.low  ) { 
      eps.low <- eps.mid
      eps.mid <- (eps.low + eps.high)/2
    }
    if (frac > frac.high ){
      eps.high <- eps.mid
      eps.mid <- (eps.low + eps.high)/2
    }
    
  }
  print(eps.mid)
  print(frac)
}


```

```{r plot fraction comets per cluster}
clsttoplot <- c(1,2,3)
p_fra <- list()
for (i in zygotes_ids[cell]) {
  spindle <- spindle_prnd_db[[i]]
  sumclst <- aggregate(spindle$vec$clst, by = list(spindle$vec$clst), FUN =  function(x) (length(x)) )
  p_fra[[i]] <- ggplotGrob(ggplot(data = sumclst) +
                             geom_bar(aes(x = Group.1, y = x, fill = factor(Group.1)),
                                      stat = 'identity') + 
                             theme_void() +
                             scale_fill_manual(name = 'cluster', values = colorUse) +
                             theme(text = element_text(size = 8),
                                   panel.grid = element_blank(),
                                   panel.background =  element_rect(color = 'white'), 
                                   legend.position = "none", 
                                   axis.text= element_blank() , 
                                   axis.ticks= element_blank() , 
                                   axis.title= element_blank()) +
                             xlab(''))
  
}

```



```{r plot clusters}
clsttoplot <- c(1,2,3)
p_clst <- list()
for (i in zygotes_ids[cell]) {
  spindle <- spindle_prnd_db[[i]]
  if (length(unique(spindle$vec$id)) < maxtrack)
    id1_s <- spindle$vec$id
  else
    id1_s <- sample(unique(spindle$vec$id), maxtrack)
  p_clst[[i]] <- ggplot(data = subset(spindle$vec, (id %in% id1_s) & (clst %in% clsttoplot) ))
  p_clst[[i]] <- p_clst[[i]] + geom_segment(aes(x = x1, xend = x2, y =  y1, yend = y2, colour = factor(clst)), alpha = 1, lineend = 'round', size = 0.22)
  p_clst[[i]] <- p_clst[[i]] + coord_fixed(ratio = 1) 
  p_clst[[i]] <- p_clst[[i]] + theme_void()
  p_clst[[i]] <- p_clst[[i]] + scale_colour_manual(name = 'cluster', values = colorUse)
  p_clst[[i]] <- p_clst[[i]] + theme(panel.background = element_rect(fill = "gray88"),
                                     legend.position = "none")
  p_clst[[i]] <- p_clst[[i]] + xlim(-maxX[i]/2, maxX[i]/2) +  ylim(-maxY[i]/2, maxY[i]/2)
  p_clst[[i]] <- p_clst[[i]] +  coord_fixed(ratio = 1) 
  p_clst[[i]] <-  p_clst[[i]] + annotation_custom(grob = p_fra[[i]], xmin = 80, xmax = 135,ymin = -135, ymax = -80)
  p_clst[[i]] = force(p_clst[[i]])
  plot(p_clst[[i]])
}

```


```{r composite }

pcomp <- cowplot::plot_grid(plotlist = p_clst[cell], ncol = 3 )
pcomp

ifelse(length(clsttoplot) == 3, 
       subfix <- '_3clst',   subfix <- '')

ggsave(plot = pcomp, file.path(datadir, sprintf('composite%s.pdf', subfix)), width = 3*image.width, height = 1.2*image.height,  unit = 'cm')

ggsave(plot = pcomp, file.path(datadir, sprintf('composite%s.png', subfix)), width = 3*image.width, height = 1.2*image.height, unit = 'cm')

```


```{r plot angles}

p_angle <- list()
p_clst2 <- list()
#maxtrack <- 100000
for (i in  zygotes_ids[1]) {
  spindle <- spindle_prnd[[i]]
  if (length(spindle$vec$id) < maxtrack)
    id1_s <- spindle$vec$id
  else
    id1_s <- sample(unique(spindle$vec$id), maxtrack)
  
  p_angle[[i]] <- ggplot(data = subset(spindle$vec, id %in% id1_s))
  p_angle[[i]] <- p_angle[[i]] + geom_segment(aes(x = x1, xend = x2, y = y1, yend = y2, colour = angle), 
                                              alpha = 0.8, lineend = 'round', size = 0.22)
  p_angle[[i]] <- p_angle[[i]] + theme_void() 
  p_angle[[i]] <- p_angle[[i]] + theme(legend.position = 'none',
                                       panel.background = element_rect(fill = "gray88")) 
  p_angle[[i]] <- p_angle[[i]] + scale_colour_gradientn(name = 'angle  ', 
                                                        limits = c(-180, 180), 
                                                        colours = rainbow(5), 
                                                        breaks = c(-180, 0, 180))
  p_angle[[i]] <- p_angle[[i]] + xlim(-maxX[i]/2, maxX[i]/2) +  ylim(-maxY[i]/2, maxY[i]/2)
  p_angle[[i]] <- p_angle[[i]] + coord_fixed(ratio = 1) 
  print(p_angle[[i]])
  
  

  p_clst2[[i]] <- ggplot(data = subset(spindle$vec, (id %in% id1_s) & (clst %in% clsttoplot) ))
  p_clst2[[i]] <- p_clst2[[i]] + geom_segment(aes(x = x1, xend = x2, y =  y1, yend = y2, colour = factor(clst)), alpha = 1, lineend = 'round', size = 0.22)
  p_clst2[[i]] <- p_clst2[[i]] + coord_fixed(ratio = 1) 
  p_clst2[[i]] <- p_clst2[[i]] + theme_void()
  p_clst2[[i]] <- p_clst2[[i]] + scale_colour_manual(name = 'cluster', values = colorUse)
  p_clst2[[i]] <- p_clst2[[i]] + theme(panel.background = element_rect(fill = "gray88"),
                                     legend.position = "none")
  p_clst2[[i]] <- p_clst2[[i]] + xlim(-maxX[i]/2, maxX[i]/2) +  ylim(-maxY[i]/2, maxY[i]/2)
  p_clst2[[i]] <- p_clst2[[i]] +  coord_fixed(ratio = 1) 

 

}




```

```{r}
legend_angle <- cowplot::get_legend(p_angle[[1]] + 
                                      theme(legend.position = "bottom", 
                                            legend.title = element_text(size  = 8),
                                            legend.text = element_text(size = 8),
                                            legend.key.height = unit(0.1, "cm"),
                                            legend.key.width = unit(0.2, "cm")))
legend_clst <- cowplot::get_legend(p_clst[[1]] + 
                                     theme(legend.position = 'bottom', 
                                           legend.title = element_text(size  = 8),
                                           legend.text = element_text(size = 8),
                                           legend.key.height = unit(0.1, "cm"),
                                           legend.key.width = unit(0.2, "cm")) +
                                     guides(color = guide_legend(override.aes =
                                                                   list(size = 1))))
spc <- 0.0
pcomp <- cowplot::plot_grid(p_angle[[1]],  p_clst2[[1]],   p_clst[[1]], legend_angle,  legend_clst,  NULL,  ncol= 3, nrow = 2, rel_heights =  c(1, 0.2))
pcomp



ggsave(plot = pcomp, file.path(datadir, sprintf('composite%s_method.pdf', subfix)), width = 2.25*image.width , height = 0.845*image.height,  unit = 'cm')

#ggsave(plot = pcomp, file.path(datadir, sprintf('composite%s_method.png', subfix)), width = 2.5*image.width , height = 1*image.height, unit = 'cm')
```


```{r run all thresholds}
# for (thrsh in seq(0.2, 0.95, 0.05)) {
#   load(file = file.path(datadir, 'spindle_prnd.RData'))
#   load(file = file.path(datadir, sprintf('eigenvectors_%.2f.RData', thrsh)))
# clust.n <- 3  # number of cluster to use
# clust.max <- 6 # maximum number of cluster to verify number of clusters
# clust.max <- min(clust.max, dim(evL[[1]]$vectors)[2])
# for (i in zygotes_ids) {
#   # if check = TRUE k mean with 1-clust.max is tested
#   km <- kmean_assign(eigen = evL[[i]], clustn = clust.n, clustmax = clust.max, check = FALSE)
#   spindle_prnd[[i]]$vec$clst <- km$cluster
# }
# 
# # Prune using additional step of density based clustering -----
# spindle_prnd_db <- list()
# frac.low  <- 0.7
# frac.high <- 0.8
# eps.low <- 1
# eps.high <- 10
# for (i in zygotes_ids[cell]) {
#   frac <- 0
#   eps.mid <- (eps.low + eps.high)/2
#   eps.high <- 10
#   eps.low <- 1
#   while(frac < frac.low  | frac > frac.high) {
#     idx_clst<- c()
#     spindle_prnd_db[[i]] <- spindle_prnd[[i]]
#     
#   
#     for (cls in seq(1, clust.n)){
#       subVec <- subset(spindle_prnd[[i]]$vec, clst == cls)
#       idx <- dbdenoise(cbind(subVec$centPos.x, subVec$centPos.y), eps = eps.mid, plotimg = FALSE)
#       idx_clst <- c(idx_clst, which(spindle_prnd[[i]]$vec$clst == cls)[idx])
#     }
#     
#     spindle_prnd_db[[i]]$vec <- spindle_prnd_db[[i]]$vec[idx_clst, ]
#     frac <- length(idx_clst)/length(spindle_prnd[[i]]$vec$clst)
#     
#     if ( frac < frac.low  ) { 
#       eps.low <- eps.mid
#       eps.mid <- (eps.low + eps.high)/2
#     }
#     if (frac > frac.high ){
#       eps.high <- eps.mid
#       eps.mid <- (eps.low + eps.high)/2
#     }
#     
#   }
#   print(eps.mid)
#   print(frac)
# }  
# 
# 
# 
# # Generate plots ----
# clsttoplot <- c(1,2,3)
# p_clst <- list()
# for (i in zygotes_ids[cell]) {
#   spindle <- spindle_prnd_db[[i]]
#   if (length(unique(spindle$vec$id)) < maxtrack)
#     id1_s <- spindle$vec$id
#   else
#     id1_s <- sample(unique(spindle$vec$id), maxtrack)
#   p_clst[[i]] <- ggplot(data = subset(spindle$vec, (id %in% id1_s) & (clst %in% clsttoplot) ))
#   p_clst[[i]] <- p_clst[[i]] + geom_segment(aes(x = x1, xend = x2, y =  y1, yend = y2, colour = factor(clst)), alpha = 1, lineend = 'round', size = 0.22)
#   p_clst[[i]] <- p_clst[[i]] + coord_fixed(ratio = 1) 
#   p_clst[[i]] <- p_clst[[i]] + theme_void()
#   p_clst[[i]] <- p_clst[[i]] + scale_colour_manual(name = 'cluster', values = colorUse)
#   p_clst[[i]] <- p_clst[[i]] + theme(panel.background = element_rect(fill = "gray88"),
#                                      legend.position = "none")
#  p_clst[[i]] <- p_clst[[i]] + xlim(-maxX[i]/2, maxX[i]/2) +  ylim(-maxY[i]/2, maxY[i]/2)
#   p_clst[[i]] <- p_clst[[i]] +  coord_fixed(ratio = 1) 
#   p_clst[[i]] = force(p_clst[[i]])
# }
# 
# # Generate composite ----
# pcomp <- cowplot::plot_grid(plotlist = p_clst[cell], ncol = 3 )
# print(pcomp)
# 
# 
# ggsave(plot = pcomp, file.path(datadir, sprintf('composite_%.2f.pdf', thrsh)), width = 3*image.width, height = 1.2*image.height,  unit = 'cm')
# 
# ggsave(plot = pcomp, file.path(datadir, sprintf('composite_%.2f.png', thrsh)), width = 3*image.width, height = 1.2*image.height, unit = 'cm')
# 
# }

```

