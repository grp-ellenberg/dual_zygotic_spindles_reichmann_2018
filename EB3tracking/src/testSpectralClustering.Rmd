---
title: "Test Spectral clustering"
output: html_notebook
---

This is an [R Markdown](http://rmarkdown.rstudio.com) Notebook. When you execute code within the notebook, the results appear beneath the code. 

Try executing this chunk by clicking the *Run* button within the chunk or by placing your cursor inside it and pressing *Ctrl+Shift+Enter*. 

```{r}
require('R.matlab') 
require('ggplot2')
library('MASS')
library('spatstat')
library('akima')
library('reshape2')
library('base')
library('proxy')
library('kernlab')
library('dbscan')
library('RSpectra')
library('RColorBrewer')
library('spdep')
source('show_comets_fun.R')

```

```{r}
#set.seed(3)
clust.n <- 6
nrvec <- 50
angleb <- runif(clust.n)*pi
angles <- c()
#angleb <- c(0, pi/4, pi/2, 3*pi/4)
for (i in seq(1, clust.n))
  angles <- c(angles, rnorm(nrvec)*0.1+angleb[i])
vecid <- as.vector(sapply(seq(1, clust.n),  FUN = function(x) (rep(x, nrvec))))

vecoffset <- as.vector(sapply(seq(1, clust.n),  FUN = function(x) (rep(x, nrvec))))
df <- data.frame(x = vecoffset + 0.1*rnorm(nrvec*clust.n), y = vecoffset + 0.1*rnorm(nrvec*clust.n), dx = cos(angles), dy = sin(angles), id = vecid)
df$x1 <- df$x 
df$x2 <- df$x + df$dx
df$y1 <- df$y
df$y2 <- df$y + df$dy

ggplot( data = df, aes(x=x, y=y)) + geom_segment(aes(xend=x+dx, yend=y+dy, color = id), arrow = arrow(length = unit(0.3,"cm")))
```


```{r create a random set of vectors}




evL <- spectclust(df, thresh = 0.9)
#evL <- spectclust_2(df, neigh = 50)

km <- kmean_assign(eigen = evL, clustn = 3, clustmax = 6, check = TRUE)

df$cls <- km$cluster
ggplot(data=df, aes(x=x, y=y)) + geom_segment(aes(xend=x+dx, yend=y+dy, colour = factor(cls)), arrow = arrow(length = unit(0.3,"cm")))

km$tot.withinss
```

Add a new chunk by clicking the *Insert Chunk* button on the toolbar or by pressing *Ctrl+Alt+I*.

When you save the notebook, an HTML file containing the code and output will be saved alongside it (click the *Preview* button or press *Ctrl+Shift+K* to preview the HTML file).
