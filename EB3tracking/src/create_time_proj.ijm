// IMAGEJ script to rotate the images by a specific angle to match the representation of the comets. 
// This is only for visualization. Angles are in degrees


// Uncomment the required lines
// index of image
i = 0
//maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/data/microtubuleball/cropped_stack/"
//angles = newArray(-142.27688 , -37.32247 , -20.78118)
//names = newArray("1/stacks.tif", "2/stacks.tif", "3/stacks.tif")

maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/data/metaphase/cropped_stack/"
angles = newArray(-27.50197, 80.21409,  -22.91831,  80.21409)
names = newArray("0/stacks_tcrop.tif", "1/stacks.tif", "2/stacks.tif", "3/stacks.tif")

//maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/data/prometaphase/cropped_stack/"
//angles = newArray(0.00000,   -50.99324, 114.59156, -74.48451, 148.96903, 180.00000, 0.00000)
//names = newArray("0/stacks.tif", "1/stacks.tif", "2/stacks.tif", "3/stacks.tif", "4/stacks.tif", "5/stacks.tif", "6/stacks.tif", "7/stacks.tif")

fname = maindir + names[i]
fname_path = File.getParent(fname)
open(fname)
run("Set Scale...", "distance=1 known=0.13 unit=um");
run("Z Project...", "projection=[Max Intensity] all");
run("Z Project...", "stop=45 projection=[Max Intensity]");
run("Rotate... ", "angle=" + angles[i]+" grid=1 interpolation=Bilinear");
makeRectangle(25, 25, 250, 250)
run("Duplicate...", "title = time_proj_stacks.tif")
run("Scale Bar...", "width=10 height=4 font=14 color=White background=None location=[Lower Right] bold hide")
//saveAs("Tiff", fname_path + "/time_proj_stacks.tif")

i = 2
maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/results/microtubuleball/"
angles = newArray(-142.27688 , -37.32247 , -20.78118)
names = newArray("1/timeproj_MAX_stacks_roi.tif", "2/timeproj_MAX_stacks_roi.tif", "3/timeproj_MAX_stacks_roi.tif")

//maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/results/metaphase/"
//angles = newArray(-27.50197, 80.21409,  -22.91831,  80.21409)
//names = newArray("0/timeproj_MAX_stacks_tcrop_roi.tif", "1/timeproj_MAX_stacks_roi.tif", "2/timeproj_MAX_stacks_roi.tif", "3/stacks.tif")

//maindir = "C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/results/prometaphase/"
//angles = newArray(0.00000,   -50.99324, 114.59156, -74.48451, 148.96903, 180.00000, 0.00000)
//names = newArray("0/timeproj_MAX_stacks_roi.tif", "1/timeproj_MAX_stacks_roi.tif", "2/timeproj_MAX_stacks_roi.tif", "3/timeproj_MAX_stacks_roi.tif", "4/stacks.tif", "5/stacks.tif", "6/stacks.tif", "7/stacks.tif")

fname = maindir + names[i]
fname_path = File.getParent(fname)
open(fname)
//open("C:/Users/toni/Dropbox/Judith_Reichmann/EB3tracking/results/metaphase/0/timeproj_MAX_stacks_tcrop_roi.tif")
run("Set Scale...", "distance=1 known=0.13 unit=um");
run("Rotate... ", "angle=" + angles[i]+" grid=1 interpolation=Bilinear");
makeRectangle(25, 25, 250, 250)
run("Duplicate...", "title = time_proj_stacks.tif")
run("Scale Bar...", "width=10 height=4 font=14 color=White background=None location=[Lower Right] bold hide")
saveAs("Tiff", fname_path + "/time_proj_MAX_stacks_rotated.tif")