# EB3 comet tracking Zygote 

[Reichmann et al. 2018](https://www.biorxiv.org/content/early/2018/04/16/198275) 

## Data preparation
Recording settings were 5 *Z*-slices, and 45-100 time points  
*xy* pixel size is 130nm
*z* interval is 48nm
time resolution is 800 msec (100 ms per slice)
NA detection 100x 1.1
NA illumination 10x 0.3

 
* Load single images in Fiji. ` File -> Import -> Image Sequence` 
* Create a hyperstack `Image -> Hyperstacks -> Stack to Hyperstack`
* Crop the image using a ROI. For all movies a ROI of 307x307 px.
* Save image as *stacks.tif* in the respective folder.
* Save each a movie per *Z*-slice, i.e. *stacks_Z1.tif*, *stacks_Z2.tif*, ...

## MATLAB processing
You need the package from Gaudenz Danuser [u-track](http://www.utsouthwestern.edu/labs/danuser/software/#utrack_anc) and make it accessible in your path. For this pipeline we used u-track-2.2.0 from June 2016.

Execute `movieSelectorGUI` this will open a GUI. Choose `u-track` on the right side.
Select `New` and add one movie per Z plane.
<p align = "center">
<img src="./resources/u-track_movie_selection.PNG" width = "500px">
</p>

Import time-lapse and specify the dimensions. Press `Save`. Repeat this operation until all Z-slices are loaded.

<p align = "center">
<img src="./resources/u-track_movie_details.PNG" width = "500px">
</p>


Press `Continue` in the `Movie selection` window. A window will open. Select `[2D] Microtubule plus tips`


<p align = "center">
<img src="./resources/u-track_object_to_detect.PNG" width = "200px">
</p>


A window will open. Check the `Step1` and `Step2`

<p align = "center">
<img src="./resources/u-track_processing_steps.PNG" width = "500px">
</p>

Change settings  for Step1 `High-pass Gaussian ...` to 3 
<p align = "center">
<img src="./resources/u-track_detection_settings1.PNG" width = "250px"><img src="./resources/u-track_detection_settings.PNG" width = "250px">
</p>

Change settings  for Step2 `Maximum Gap to Close` to 1. Check `Export tracking results to matrix format`. 

<p align = "center">
<img src="./resources/u-track_tracking_settings.PNG" width = "500px">
</p>

In the control panel press `Run` for all movies.
> BUGS: Not all movies are processed at once. If an error occurs (tipically connected to rmdir) run all files again. This may require 2-3 runs. The gap histogram are often blank, but this is normal for the settings used here.


## ImageJ Kymograph etc.
To generate the Kymographs images were filtered using a Laplacian of Gaussian ([LoG3D](http://bigwww.epfl.ch/sage/soft/LoG3D/) sigmaX 2, sigmaY 2, process single planes)

The Kymograph was generated with [MultiKymograph ](https://imagej.net/Multi_Kymograph) using a line width of 11 (line length ~ 211 pxs)
